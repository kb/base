# -*- encoding: utf-8 -*-
import os.path

from django.db import models

from base.model_utils import (
    RetryModel,
    RetryModelManager,
    TimedCreateModifyDeleteArchiveModel,
    TimedCreateModifyDeleteModel,
    TimedCreateModifyDeleteVersionModel,
    TimedCreateModifyDeleteVersionModelManager,
)


class AppleCake(TimedCreateModifyDeleteVersionModel):
    """This model generates unique numbers per category."""

    UNIQUE_FIELD_NAME = "number"
    UNIQUE_FIELD_NAME_CATEGORY = "category"

    description = models.CharField(max_length=100)
    category = models.CharField(max_length=10)
    number = models.IntegerField(default=0)
    quantity = models.IntegerField()
    objects = TimedCreateModifyDeleteVersionModelManager()

    class Meta:
        ordering = ("description",)
        unique_together = ("number", "deleted_version", "category")
        verbose_name = "Apple Cake"
        verbose_name_plural = "Apple Cakes"

    def __str__(self):
        return "{}".format(self.description)


class FruitCake(TimedCreateModifyDeleteVersionModel):
    """This model generates unique numbers, and has no category."""

    UNIQUE_FIELD_NAME = "number"

    description = models.CharField(max_length=100)
    number = models.IntegerField(default=0)
    quantity = models.IntegerField()
    objects = TimedCreateModifyDeleteVersionModelManager()

    class Meta:
        ordering = ("description",)
        unique_together = ("number", "deleted_version")
        verbose_name = "Fruit Cake"
        verbose_name_plural = "Fruit Cakes"

    def __str__(self):
        return "{}".format(self.description)


class CoffeeCake(TimedCreateModifyDeleteArchiveModel):
    description = models.CharField(max_length=100)
    quantity = models.IntegerField()

    class Meta:
        ordering = ("description",)
        verbose_name = "Coffee Cake"
        verbose_name_plural = "Coffee Cakes"

    def __str__(self):
        return f"{description}"


class LemonCake(TimedCreateModifyDeleteModel):
    description = models.CharField(max_length=100)
    quantity = models.IntegerField()

    class Meta:
        ordering = ("description",)
        verbose_name = "Lemon Cake"
        verbose_name_plural = "Lemon Cakes"

    def __str__(self):
        return "{}".format(self.description)


class Document(TimedCreateModifyDeleteModel):
    """Example model for the 'FileDropInput' widget."""

    file = models.FileField(upload_to="document")
    preview = models.FileField(upload_to="image", blank=True, null=True)
    description = models.CharField(max_length=256)

    class Meta:
        ordering = ("id",)
        verbose_name = "Document"
        verbose_name_plural = "Documents"

    def __str__(self):
        return "{}: {}".format(self.file, self.description)

    def filename(self):
        if self.file and self.file.name:
            return os.path.basename(self.file.name)
        return ""

    def is_image(self):
        import imghdr
        from django.conf import settings

        try:
            if imghdr.what(os.path.join(settings.MEDIA_ROOT, self.file.name)):
                return True
        except:
            pass

        return False


class RetryTaskModelManager(RetryModelManager):
    def current(self):
        return self.model.objects.all()


class RetryTask(RetryModel):
    """Test the retry model."""

    name = models.CharField(max_length=20)
    objects = RetryTaskModelManager()

    class Meta:
        verbose_name = "Retry Task"

    def __str__(self):
        retry_str = self.retry_str()
        if retry_str:
            result = "{} ({})".format(self.name, retry_str)
        else:
            result = self.name
        return result

    def default_retry_count():
        return 5

    def process(self):
        """Process the task.

        .. note:: This method is running inside a transaction.

        This method is called by ``RetryModelManager``
        (see ``base/model_utils.py``).

        """
        return True
