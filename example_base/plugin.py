# -*- encoding: utf-8 -*-


class FruitBaseMixin:
    def get_context_data(self_for_instance):
        """Add extra context to 'BaseMixin'...

        For docs, see:
        https://www.kbsoftware.co.uk/docs/app-apps.html#views

        Ticket:
        https://www.kbsoftware.co.uk/crm/ticket/4777/

        """
        return {"fruit": "Apple"}
