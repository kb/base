# -*- encoding: utf-8 -*-
import pytest

from .factories import CoffeeCakeFactory


@pytest.mark.django_db
def test_factory():
    CoffeeCakeFactory()


@pytest.mark.django_db
@pytest.mark.django_db
def test_set_archived():
    cake = CoffeeCakeFactory()
    assert cake.is_archived is False
    cake.set_archived()
    assert cake.is_archived is True


@pytest.mark.django_db
def test_set_unarchive():
    cake = CoffeeCakeFactory(archived=True)
    assert cake.is_archived is True
    cake.unarchive()
    assert cake.is_archived is False
