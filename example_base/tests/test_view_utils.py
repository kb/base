# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus


@pytest.mark.django_db
def test_base_mixin_context_plugin(client, settings):
    settings.BASE_MIXIN_CONTEXT_PLUGIN = ["example_base.plugin.FruitBaseMixin"]
    response = client.get(reverse("project.home"))
    assert HTTPStatus.OK == response.status_code
    assert "fruit" in response.context
    assert "Apple" == response.context["fruit"]


@pytest.mark.django_db
def test_base_mixin_context_plugin_no_settings(client, settings):
    with pytest.raises(AttributeError) as e:
        settings.BASE_MIXIN_CONTEXT_PLUGIN
    assert (
        "'Settings' object has no attribute 'BASE_MIXIN_CONTEXT_PLUGIN'"
        in str(e.value)
    )
    response = client.get(reverse("project.home"))
    assert HTTPStatus.OK == response.status_code
    assert "fruit" not in response.context
