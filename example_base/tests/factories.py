# -*- encoding: utf-8 -*-
import factory

from example_base.models import (
    AppleCake,
    CoffeeCake,
    FruitCake,
    LemonCake,
    RetryTask,
)


class AppleCakeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AppleCake

    quantity = 1

    @factory.sequence
    def number(n):
        return n + 1


class CoffeeCakeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CoffeeCake

    quantity = 1


class FruitCakeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = FruitCake

    quantity = 1

    @factory.sequence
    def number(n):
        return n + 1


class LemonCakeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = LemonCake

    quantity = 1


class RetryTaskFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = RetryTask

    max_retry_count = 5

    @factory.sequence
    def name(n):
        return "name_{}".format(n + 1)
