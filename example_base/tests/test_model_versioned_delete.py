# -*- encoding: utf-8 -*-
import pytest

from django.utils import timezone

from base.model_utils import BaseError
from example_base.models import AppleCake, FruitCake
from login.tests.factories import UserFactory
from .factories import AppleCakeFactory, FruitCakeFactory


@pytest.mark.parametrize("factory_class", [AppleCakeFactory, FruitCakeFactory])
@pytest.mark.django_db
def test_factory(factory_class):
    factory_class()


@pytest.mark.parametrize("model_class", [AppleCake, FruitCake])
@pytest.mark.django_db
def test_next_number(model_class):
    user = UserFactory()
    for description in ("a", "b", "c", "d", "e", "f"):
        cake = model_class(
            description=description,
            quantity=1,
            number=model_class.objects.next_number(),
        )
        cake.save()
    qs = model_class.objects.all().order_by("description")
    assert [(1, 0), (2, 0), (3, 0), (4, 0), (5, 0), (6, 0)] == [
        (x.number, x.deleted_version) for x in qs
    ]
    # for description in ('a', 'b', 'c', 'd', 'e', 'f'):
    #     cake = model_class.objects.get(description=description)
    #     print(cake.description, cake.number, cake.deleted_version)

    # check
    cake = model_class.objects.get(description="c")
    assert 3 == cake.number
    # delete cake 'b'
    cake = model_class.objects.get(description="b")
    model_class.objects.set_deleted(cake, user)
    # cake.set_deleted(user)
    # delete cake 'd'
    cake = model_class.objects.get(description="d")
    model_class.objects.set_deleted(cake, user)
    # cake.set_deleted(user)

    # for description in ('a', 'b', 'c', 'd', 'e', 'f'):
    #     cake = model_class.objects.get(description=description)
    #     print(
    #         cake.description,
    #         cake.number,
    #         cake.deleted_version,
    #         cake.is_deleted,
    #     )

    # cakes 2 and 4 have been deleted
    qs = model_class.objects.all().order_by("description")
    assert [
        ("a", 1, 0, False),
        ("b", 2, 1, True),
        ("c", 3, 0, False),
        ("d", 4, 1, True),
        ("e", 5, 0, False),
        ("f", 6, 0, False),
    ] == [(x.description, x.number, x.deleted_version, x.deleted) for x in qs]

    # make sure we re-use the deleted number
    next_number = model_class.objects.next_number()
    assert 2 == next_number
    cake = model_class(description="g", quantity=1, number=next_number)
    cake.save()
    # make sure we re-use the deleted number
    next_number = model_class.objects.next_number()
    assert 4 == next_number
    cake = model_class(description="h", quantity=1, number=next_number)
    cake.save()
    # make sure we generate a new number
    next_number = model_class.objects.next_number()
    assert 7 == next_number
    cake = model_class(description="i", quantity=1, number=next_number)
    cake.save()
    qs = model_class.objects.all().order_by("description")
    # delete 'h' again i.e. delete number 4 again
    cake = model_class.objects.get(description="h")
    model_class.objects.set_deleted(cake, user)
    # final check
    assert [
        ("a", 1, 0, False),
        ("b", 2, 1, True),
        ("c", 3, 0, False),
        ("d", 4, 1, True),
        ("e", 5, 0, False),
        ("f", 6, 0, False),
        ("g", 2, 0, False),
        ("h", 4, 2, True),
        ("i", 7, 0, False),
    ] == [(x.description, x.number, x.deleted_version, x.deleted) for x in qs]


@pytest.mark.django_db
def test_next_number_category():
    user = UserFactory()
    for description in ("a", "b", "c"):
        category = "pippin"
        number = AppleCake.objects.next_number(category)
        cake = AppleCake(
            category=category,
            description=description,
            quantity=1,
            number=number,
        )
        cake.save()
    for description in ("d", "e", "f"):
        category = "smith"
        number = AppleCake.objects.next_number(category)
        cake = AppleCake(
            category=category,
            description=description,
            quantity=1,
            number=number,
        )
        cake.save()
    # delete cake 'b'
    cake = AppleCake.objects.get(description="b")
    AppleCake.objects.set_deleted(cake, user)
    # delete cake 'd'
    cake = AppleCake.objects.get(description="d")
    AppleCake.objects.set_deleted(cake, user)
    # re-use numbers
    number = AppleCake.objects.next_number("smith")
    cake = AppleCake(
        category="smith", description="g", quantity=1, number=number
    )
    cake.save()
    # re-use numbers
    number = AppleCake.objects.next_number("pippin")
    cake = AppleCake(
        category="pippin", description="h", quantity=1, number=number
    )
    cake.save()
    # new
    number = AppleCake.objects.next_number("smith")
    cake = AppleCake(
        category="smith", description="i", quantity=1, number=number
    )
    cake.save()
    # new
    number = AppleCake.objects.next_number("pippin")
    cake = AppleCake(
        category="pippin", description="j", quantity=1, number=number
    )
    cake.save()
    # new
    number = AppleCake.objects.next_number("smith")
    cake = AppleCake(
        category="smith", description="k", quantity=1, number=number
    )
    cake.save()

    qs = AppleCake.objects.all().order_by("description")
    assert [
        ("a", "pippin", 1, 0, False),
        ("b", "pippin", 2, 1, True),
        ("c", "pippin", 3, 0, False),
        ("d", "smith", 1, 1, True),
        ("e", "smith", 2, 0, False),
        ("f", "smith", 3, 0, False),
        ("g", "smith", 1, 0, False),
        ("h", "pippin", 2, 0, False),
        ("i", "smith", 4, 0, False),
        ("j", "pippin", 4, 0, False),
        ("k", "smith", 5, 0, False),
    ] == [
        (x.description, x.category, x.number, x.deleted_version, x.deleted)
        for x in qs
    ]


@pytest.mark.parametrize("factory_class", [AppleCakeFactory, FruitCakeFactory])
@pytest.mark.django_db
def test_str(factory_class):
    str(factory_class())


@pytest.mark.parametrize(
    "factory_class,model_class",
    [(AppleCakeFactory, AppleCake), (FruitCakeFactory, FruitCake)],
)
@pytest.mark.django_db
def test_set_deleted(factory_class, model_class):
    obj = factory_class(number=99)
    assert 0 == obj.deleted_version
    user = UserFactory()
    model_class.objects.set_deleted(obj, user)
    # obj.set_deleted(user)
    obj.refresh_from_db()
    assert 99 == obj.number
    assert 1 == obj.deleted_version
    assert obj.deleted is True
    assert obj.user_deleted == user
    assert timezone.now().date() == obj.date_deleted.date()


@pytest.mark.parametrize(
    "factory_class,model_class",
    [(AppleCakeFactory, AppleCake), (FruitCakeFactory, FruitCake)],
)
@pytest.mark.django_db
def test_set_deleted_multi(factory_class, model_class):
    user = UserFactory()
    c1 = factory_class(number=19, description="c1")
    model_class.objects.set_deleted(c1, user)
    # c1.set_deleted(user)
    c2 = factory_class(number=19, description="c2")
    model_class.objects.set_deleted(c2, user)
    # c2.set_deleted(user)
    factory_class(number=19, description="c3")
    result = model_class.objects.filter(number=19).order_by("description")
    assert 3 == result.count()
    assert [(19, True, 1), (19, True, 2), (19, False, 0)] == [
        (o.number, o.deleted, o.deleted_version) for o in result
    ]


@pytest.mark.parametrize(
    "factory_class,model_class",
    [(AppleCakeFactory, AppleCake), (FruitCakeFactory, FruitCake)],
)
@pytest.mark.django_db
def test_set_deleted_model(factory_class, model_class):
    """Standard way to delete rows from non-versioned delete models"""
    obj = factory_class()
    with pytest.raises(BaseError) as e:
        obj.set_deleted(UserFactory())
    assert "model with deleted version tracking" in str(e.value)


@pytest.mark.parametrize(
    "factory_class,model_class",
    [(AppleCakeFactory, AppleCake), (FruitCakeFactory, FruitCake)],
)
@pytest.mark.django_db
def test_undelete(factory_class, model_class):
    obj = factory_class()
    user = UserFactory()
    model_class.objects.set_deleted(obj, user)
    assert obj.deleted_version > 0
    obj.refresh_from_db()
    obj.undelete()
    obj.refresh_from_db()
    assert obj.deleted is False
    assert obj.user_deleted is None
    assert obj.date_deleted is None
    assert 0 == obj.deleted_version
