# -*- encoding: utf-8 -*-
import pytest

from django.contrib.auth import REDIRECT_FIELD_NAME
from django.urls import reverse
from http import HTTPStatus

from base.url_utils import url_with_querystring
from login.tests.factories import TEST_PASSWORD, UserFactory


@pytest.mark.django_db
def test_redirect_next_mixin(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("project.dash")
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert REDIRECT_FIELD_NAME not in response.context


@pytest.mark.django_db
def test_redirect_next_mixin_iri_to_uri(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = "{}?next=/café/".format(reverse("project.dash"))
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert REDIRECT_FIELD_NAME in response.context
    assert "/caf%C3%A9/" == response.context[REDIRECT_FIELD_NAME]


@pytest.mark.django_db
def test_redirect_next_mixin_next(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    parameters = {REDIRECT_FIELD_NAME: reverse("project.settings")}
    url = url_with_querystring(reverse("project.dash"), **parameters)
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert REDIRECT_FIELD_NAME in response.context
    assert reverse("project.settings") == response.context[REDIRECT_FIELD_NAME]


@pytest.mark.django_db
def test_redirect_next_mixin_next_with_extra_parameters(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    parameters = {"fruit": "Apple", "vegetable": "Swede"}
    next_url = url_with_querystring(reverse("project.settings"), **parameters)
    response = client.get(
        url_with_querystring(
            reverse("project.dash"), **{REDIRECT_FIELD_NAME: next_url}
        )
    )
    assert HTTPStatus.OK == response.status_code
    assert REDIRECT_FIELD_NAME in response.context
    assert next_url == response.context[REDIRECT_FIELD_NAME]


@pytest.mark.django_db
def test_redirect_next_mixin_xss(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = "{}?next=javascript:alert(1)".format(reverse("project.dash"))
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert REDIRECT_FIELD_NAME not in response.context
