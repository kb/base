# -*- encoding: utf-8 -*-
import pytest

from django.utils import timezone
from freezegun import freeze_time

from example_base.models import RetryTask
from example_base.tests.factories import RetryTaskFactory


@pytest.mark.django_db
@pytest.mark.parametrize(
    "completed_date,max_retry_count,expect",
    [
        (None, 3, False),
        (None, 4, False),
        (None, 5, True),
        (timezone.now(), 3, False),
        (timezone.now(), 4, False),
        (timezone.now(), 5, False),
    ],
)
def test_can_process(completed_date, max_retry_count, expect):
    x = RetryTaskFactory(
        completed_date=completed_date,
        max_retry_count=max_retry_count,
        retries=4,
    )
    assert x.can_process() is expect


@pytest.mark.django_db
def test_clazz_name():
    assert "RetryTaskModelManager" == RetryTask.objects._clazz_name()


@pytest.mark.django_db
def test_completed():
    RetryTaskFactory(name="a", retries=4)
    RetryTaskFactory(name="b", retries=99)
    RetryTaskFactory(name="c", completed_date=timezone.now())
    RetryTaskFactory(name="d")
    RetryTaskFactory(name="e", retries=5)
    assert ["c"] == [x.name for x in RetryTask.objects.completed()]


@pytest.mark.django_db
def test_is_complete():
    x = RetryTaskFactory()
    assert x.is_complete() is False
    x = RetryTaskFactory(completed_date=timezone.now())
    assert x.is_complete() is True


@pytest.mark.django_db
def test_outstanding():
    RetryTaskFactory(name="a", retries=4)
    RetryTaskFactory(name="b", retries=99)
    RetryTaskFactory(name="c", completed_date=timezone.now())
    RetryTaskFactory(name="d", queue_name="")
    RetryTaskFactory(name="e", retries=5)
    RetryTaskFactory(name="f", retries=99, max_retry_count=100)
    # only include tasks where the queue name is empty
    RetryTaskFactory(name="g", queue_name="my-queue-name")
    assert ["a", "d", "f"] == [x.name for x in RetryTask.objects.outstanding()]


@pytest.mark.django_db
def test_outstanding_ignore_retries():
    RetryTaskFactory(name="a", retries=4)
    RetryTaskFactory(name="b", retries=99)
    RetryTaskFactory(name="c", completed_date=timezone.now())
    RetryTaskFactory(name="d")
    RetryTaskFactory(name="e", retries=5)
    assert ["a", "b", "d", "e"] == [
        x.name for x in RetryTask.objects.outstanding_ignore_retries()
    ]


@pytest.mark.django_db
def test_outstanding_queue_name():
    RetryTaskFactory(name="a", queue_name="my-queue-name", retries=4)
    RetryTaskFactory(name="b", queue_name="my-queue-name", retries=99)
    RetryTaskFactory(
        name="c", queue_name="my-queue-name", completed_date=timezone.now()
    )
    RetryTaskFactory(name="d", queue_name="my-queue-name")
    RetryTaskFactory(name="e", queue_name="my-queue-name", retries=5)
    RetryTaskFactory(
        name="f", queue_name="my-queue-name", retries=99, max_retry_count=100
    )
    # only include tasks which have a queue name
    RetryTaskFactory(name="g", queue_name="")
    assert ["a", "d", "f"] == [
        x.name for x in RetryTask.objects.outstanding("my-queue-name")
    ]


@pytest.mark.django_db
def test_process():
    task_a = RetryTaskFactory(name="a", retries=4)
    RetryTaskFactory(name="b", retries=99)
    RetryTaskFactory(name="c", completed_date=timezone.now())
    task_d = RetryTaskFactory(name="d")
    RetryTaskFactory(name="e", retries=5)
    RetryTaskFactory(name="f", queue_name="my-queue-name", retries=4)
    RetryTaskFactory(name="g", queue_name="my-queue-name")
    assert task_a.completed_date is None
    assert task_d.completed_date is None
    result = RetryTask.objects.process()
    assert [task_a.pk, task_d.pk] == result
    # check they are all marked as complete
    for pk in result:
        task = RetryTask.objects.get(pk=pk)
        assert timezone.now().date() == task.completed_date.date()


@pytest.mark.django_db
def test_process_pk():
    task_a = RetryTaskFactory(name="a", retries=4)
    RetryTaskFactory(name="b", retries=99)
    RetryTaskFactory(name="c", completed_date=timezone.now())
    task_d = RetryTaskFactory(name="d")
    RetryTaskFactory(name="e", retries=5)
    assert task_a.completed_date is None
    assert task_d.completed_date is None
    result = RetryTask.objects.process(task_d.pk)
    assert [task_d.pk] == result
    # check they are all marked as complete
    for pk in result:
        task = RetryTask.objects.get(pk=pk)
        assert timezone.now().date() == task.completed_date.date()


@pytest.mark.django_db
def test_process_queue_name():
    task_a = RetryTaskFactory(name="a", queue_name="q", retries=4)
    RetryTaskFactory(name="b", queue_name="q", retries=99)
    RetryTaskFactory(name="c", queue_name="q", completed_date=timezone.now())
    task_d = RetryTaskFactory(name="d", queue_name="q")
    RetryTaskFactory(name="e", queue_name="q", retries=5)
    RetryTaskFactory(name="f", queue_name="", retries=4)
    RetryTaskFactory(name="g", queue_name="")
    assert task_a.completed_date is None
    assert task_d.completed_date is None
    result = RetryTask.objects.process(queue_name="q")
    assert [task_a.pk, task_d.pk] == result
    # check they are all marked as complete
    for pk in result:
        task = RetryTask.objects.get(pk=pk)
        assert timezone.now().date() == task.completed_date.date()


@pytest.mark.django_db
def test_set_complete():
    task = RetryTaskFactory(name="Patrick", retries=3)
    assert 0 == RetryTask.objects.completed().count()
    task.set_complete()
    assert 1 == RetryTask.objects.completed().count()


@pytest.mark.django_db
def test_set_fail():
    task = RetryTaskFactory(name="Patrick", retries=3)
    task.set_fail()
    task.refresh_from_db()
    assert 4 == task.retries
    assert timezone.now().date() == task.fail_date.date()


@pytest.mark.django_db
def test_set_fail_already_complete():
    task = RetryTaskFactory(name="Patrick", retries=3)
    assert task.is_complete() is False
    task.set_complete()
    task.refresh_from_db()
    assert task.is_complete() is True
    task.set_fail()
    task.refresh_from_db()
    assert task.is_complete() is True


@pytest.mark.django_db
def test_str():
    task = RetryTaskFactory(name="Patrick", retries=3)
    assert "Patrick (retry x 3)" == str(task)


@pytest.mark.django_db
def test_str_completed():
    task = RetryTaskFactory(name="Patrick")
    with freeze_time("2017-05-21 23:55:01"):
        task.set_complete()
    assert "Patrick (completed 21/05/2017 23:55)" == str(task)
