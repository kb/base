# -*- encoding: utf-8 -*-
from django import forms
from base.form_utils import RequiredFieldForm, FileDropInput
from .models import Document


class DocumentForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        """
        # if we were not using RequiredFieldForm we could set the widget as
        # follows (or use the widget attribute of the Meta class):

        # First field is assigns zone_id automatically

        self.fields['file'].widget = FileDropInput()

        # For the second field we need to specify a unique field name
        # using the name filedrop-1-zone means we do not need to have any
        # custom css or js code.

        # This example also shows how to set up a new instance of the widget
        # to change the default_text and click_text

        self.fields['preview'].widget = FileDropInput(
            zone_id="filedrop-1-zone",
            default_text="Drop a preview image file...",
            click_text="or click here to choose one"
        )

        # but you can simply update the attrs properties as follows:
        """

        self.fields["preview"].widget.attrs.update(
            {
                "default_text": "Drop a preview image file...",
                "click_text": "or click here to choose",
            }
        )

        for name in ("file", "preview", "description"):
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})

    class Meta:
        model = Document
        fields = ("file", "preview", "description")

        # RequiredFieldForm uses FileDropInput for all FileFields and
        # ImageFields so the widgets attribute is unnecessary and will be
        # superceded by values assigned in the RequiredFieldForm's __init__
        # method.  To change the zone_id, default_text and click_text assign
        # them in __init__ (see above)
        # widgets = {
        #     'file': FileDropInput(),
        #     'preview': FileDropInput(
        #         zone_id="this-is-superceded",
        #         default_text="This is superceded",
        #         click_text="This is superceded",
        #     )
        # }


# this is an example of how to use in a basic ModelForm
class BasicDocumentModelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for name in ("file", "preview", "description"):
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})

    class Meta:
        model = Document
        fields = ("file", "preview", "description")

        widgets = {
            "file": FileDropInput(),
            "preview": FileDropInput(
                zone_id="filedrop-1-zone",
                default_text="Drop a preview image file...",
                click_text="or click here to choose one",
            ),
        }


# this is an example of how to use in a basic Form
class NonModelForm(forms.Form):
    file = forms.FileField(widget=FileDropInput)
    description = forms.CharField(max_length=200)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in ("file", "description"):
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})

    class Meta:
        widgets = {"file": FileDropInput()}
