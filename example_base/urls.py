# -*- encoding: utf-8 -*-
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, re_path

from .views import (
    HomeView,
    DashView,
    SettingsView,
    FileDropDemoView,
    AjaxFileUploadView,
)


urlpatterns = [
    re_path(r"^$", view=HomeView.as_view(), name="project.home"),
    re_path(r"^dash/$", view=DashView.as_view(), name="project.dash"),
    re_path(
        r"^settings/",
        view=SettingsView.as_view(),
        name="project.settings",
    ),
    re_path(r"^", view=include("login.urls")),
    re_path(
        r"^dash/filedrop/",
        view=FileDropDemoView.as_view(),
        name="filedrop.demo",
    ),
    re_path(
        r"^ajax-upload/$",
        view=AjaxFileUploadView.as_view(),
        name="ajax.file.upload",
    ),
]

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        re_path(r"^__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
