source venv-base/bin/activate.fish
if command -q k3d
  echo "Using Kubernetes"
  set -x KUBECONFIG (k3d get-kubeconfig)
  set -x DATABASE_HOST (kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
  set -x DATABASE_PASS "postgres"
  set -x DATABASE_PORT (kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services kb-dev-db-postgresql)
  set -x REDIS_HOST (kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
  set -x REDIS_PORT (kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services kb-redis-master)
  echo "KUBECONFIG:" $KUBECONFIG
else
  set -x DATABASE_HOST ""
  set -x DATABASE_PASS ""
  set -x DATABASE_PORT ""
  set -x REDIS_HOST "localhost"
  set -x REDIS_PORT "6379"
end

set -x DATABASE_NAME "dev_app_base_$USER"
set -x DATABASE_USER "postgres"
set -x DJANGO_SETTINGS_MODULE "example_base.dev_$USER"
set -x DOMAIN "dev"
set -x LOG_FOLDER ""
set -x LOG_SUFFIX "dev"
set -x USE_OPENID_CONNECT "False"

source .private

echo "DATABASE_NAME:" $DATABASE_NAME
echo "DATABASE_HOST:" $DATABASE_HOST
echo "DATABASE_PASS:" $DATABASE_PASS
echo "DATABASE_PORT:" $DATABASE_PORT
echo "DJANGO_SETTINGS_MODULE:" $DJANGO_SETTINGS_MODULE
