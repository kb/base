base
****

Django base application
https://www.kbsoftware.co.uk/docs/app-base.html

Install
=======

Virtual Environment
-------------------

::

  python3 -m venv venv-base
  # or
  virtualenv --python=python3 venv-base

  source venv-base/bin/activate

  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  pytest -x

Usage
=====

::

  ./init_dev.sh

Release
=======

https://www.kbsoftware.co.uk/docs/
