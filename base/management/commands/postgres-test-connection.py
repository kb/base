# -*- encoding: utf-8 -*-
import psycopg

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    """Test a postgres connection using 'psycopg'.

    Documentation,
    https://www.kbsoftware.co.uk/docs/app-base.html#postgres-test-connection

    Code copied from, *Main objects in Psycopg 3*
    https://www.psycopg.org/psycopg3/docs/basic/usage.html#main-objects-in-psycopg-3

    For Power BI - export data:
    https://www.kbsoftware.co.uk/crm/ticket/5089/

    .. tip:: I was able to connect without a password, but not sure why or how!
             You may need to add the ``pass`` argument...

    """

    help = "Test postgres connection #5089"

    def add_arguments(self, parser):
        parser.add_argument("-host", type=str, nargs="?")
        parser.add_argument("-port", type=str, nargs="?")
        parser.add_argument("-name", type=str)
        parser.add_argument("-user", type=str)
        parser.add_argument("-pass", type=str, nargs="?")

    def handle(self, *args, **options):
        self.stdout.write(self.help)
        host = options["host"]
        port = options["port"]
        name = options["name"]
        user = options["user"]
        password = options.get("pass")
        if password:
            connection_string = (
                f"host={host} port={port} dbname={name} "
                f"user={user} password={password}"
            )
        elif port:
            connection_string = f"port={port} dbname={name} user={user}"
        else:
            # Just avoid the host and it doesn't do the password checking...
            # https://www.peterbe.com/plog/connecting-with-psycopg2-without-a-username-and-password
            connection_string = f"dbname={name} user={user}"
        self.stdout.write(f"connection_string: {connection_string}")
        with psycopg.connect(connection_string) as conn:
            self.stdout.write(f"'{user}' connected to '{name}'")

        self.stdout.write(f"{self.help} - Complete")
