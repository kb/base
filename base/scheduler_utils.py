# -*- encoding: utf-8 -*-
import logging

from pytz import utc
from apscheduler.schedulers.background import BlockingScheduler


logger = logging.getLogger(__name__)


def create_scheduler():
    job_defaults = {"coalesce": True, "max_instances": 1}
    logger.info("Scheduler creating...")
    scheduler = BlockingScheduler(job_defaults=job_defaults, timezone=utc)
    logger.info("Scheduler created.")
    return scheduler
