# -*- encoding: utf-8 -*-
from django.core.management import call_command
from django.core.management.base import BaseCommand


class DramatiqBaseCommandMixin(BaseCommand):
    """Start Dramatiq workers - using the correct queue name.

    https://github.com/Bogdanp/django_dramatiq/blob/master/django_dramatiq/management/commands/rundramatiq.py

    """

    help = "Start Dramatiq workers..."

    def get_queue_name(self):
        raise NotImplementedError(
            "'{}' must provide an implementation of 'get_queue_name'".format(
                self.__class__.__module__
            )
        )

    def add_arguments(self, parser):
        parser.add_argument(
            "--reload",
            action="store_true",
            default=False,
            help="Reload the code when you make a change",
        )

    def handle(self, *args, **options):
        self.stdout.write("{}...".format(self.help))
        parameters = [
            "rundramatiq",
            "--processes={}".format(self.PROCESSES),
            "--queues={}".format(self.get_queue_name()),
        ]
        if options["reload"]:
            parameters.append("--reload")
            self.stdout.write(" * Reload (when the code changes)...")
        call_command(*parameters)
        self.stdout.write("{} - Complete".format(self.help))
