# -*- encoding: utf-8 -*-
from base.form_utils import bleach_clean


def test_clean_form():
    description = "<form><p>Apple</p></form>"
    assert "&lt;form&gt;<p>Apple</p>&lt;/form&gt;" == bleach_clean(description)


def test_clean_form_strip():
    description = "<form><p>Apple</p></form>"
    assert "<p>Apple</p>" == bleach_clean(description, strip=True)


def test_default():
    description = "Hot, hot, hot..."
    assert "Hot, hot, hot..." == bleach_clean(description)


def test_image():
    description = (
        '<img alt="" src="http://rodgersindexlarge.jpg" '
        'style="float:right; height:90px; width:160px"/>'
    )
    clean = bleach_clean(description)
    assert "<img" in clean
    assert "src" in clean
    assert "style" in clean
    assert "float" in clean
    assert "right" in clean
    assert "height" in clean
    assert "width" in clean
    assert "alt" in clean


def test_link():
    description = (
        '<a target="_blank" href="http://bbc.co.uk">http://bbc.co.uk</a>'
    )
    clean = bleach_clean(description)
    assert "<a" in clean
    assert "href" in clean
    assert "target" in clean


def test_strong():
    description = "Hot, <strong>hot</strong>, hot..."
    assert "Hot, <strong>hot</strong>, hot..." == bleach_clean(description)


def test_underline():
    description = "Hot, <u>hot</u>, hot..."
    assert "Hot, <u>hot</u>, hot..." == bleach_clean(description)


def test_youtube():
    description = (
        '<iframe width="640" height="360" '
        'src="//www.youtube.com/embed/UB-DhUIvcac?rel=0" '
        'frameborder="0" allowfullscreen=""></iframe>'
    )
    clean = bleach_clean(description)
    assert "<iframe" in clean
    assert "allowfullscreen" in clean
    assert "frameborder" in clean
    assert "height" in clean
    assert "src" in clean
    assert "width" in clean
