# -*- encoding: utf-8 -*-
import pytest

from base.view_utils import ElasticPaginator


@pytest.mark.parametrize(
    "page_number,page_size,total,expect", [(1, 3, 4, True), (1, 3, 3, False)]
)
def test_has_next(page_number, page_size, total, expect):
    paginator = ElasticPaginator(page_number, page_size, total)
    assert paginator.has_next is expect


@pytest.mark.parametrize(
    "page_number,page_size,total,expect", [(1, 3, 4, True), (1, 3, 3, False)]
)
def test_has_other_pages(page_number, page_size, total, expect):
    paginator = ElasticPaginator(page_number, page_size, total)
    assert paginator.has_other_pages() is expect


@pytest.mark.parametrize(
    "page_number,page_size,total,expect", [(2, 3, 4, True), (1, 3, 4, False)]
)
def test_has_previous(page_number, page_size, total, expect):
    paginator = ElasticPaginator(page_number, page_size, total)
    assert paginator.has_previous is expect


@pytest.mark.parametrize(
    "page_number,page_size,total,expect", [(2, 3, 4, 2), (1, 3, 4, 2)]
)
def test_next_page_number(page_number, page_size, total, expect):
    paginator = ElasticPaginator(page_number, page_size, total)
    assert expect == paginator.next_page_number


@pytest.mark.parametrize(
    "page_number,page_size,total,expect", [(2, 3, 4, 2), (1, 3, 4, 1)]
)
def test_number(page_number, page_size, total, expect):
    paginator = ElasticPaginator(page_number, page_size, total)
    assert expect == paginator.number


@pytest.mark.parametrize(
    "page_number,page_size,total,expect", [(2, 3, 4, 2), (1, 3, 3, 1)]
)
def test_paginator(page_number, page_size, total, expect):
    paginator = ElasticPaginator(page_number, page_size, total)
    assert expect == paginator.paginator.num_pages


@pytest.mark.parametrize(
    "page_number,page_size,total,expect", [(2, 3, 4, 1), (1, 3, 4, 1)]
)
def test_previous_page_number(page_number, page_size, total, expect):
    paginator = ElasticPaginator(page_number, page_size, total)
    assert expect == paginator.previous_page_number
