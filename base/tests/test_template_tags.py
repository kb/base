# -*- encoding: utf-8 -*-
import collections
import pytest

from django import forms
from django.contrib.auth.models import Group

from base.templatetags.base_tags import (
    is_checkbox,
    is_checkbox_select_multiple,
    is_file_input,
    is_radio_select,
    is_select_multiple,
)


# Use a ``namedtuple`` to set-up the test data
FormData = collections.namedtuple("FormData", "field")


def test_is_checkbox():
    field = forms.BooleanField()
    assert is_checkbox(FormData(field=field)) is True


def test_is_checkbox_select_multiple():
    field = forms.ModelMultipleChoiceField(
        queryset=Group.objects.none(), widget=forms.CheckboxSelectMultiple
    )
    assert is_checkbox_select_multiple(FormData(field=field)) is True


def test_is_file_input():
    field = forms.FileField(widget=forms.FileInput)
    assert is_file_input(FormData(field=field)) is True


def test_is_radio_select():
    field = forms.ChoiceField(widget=forms.RadioSelect)
    assert is_radio_select(FormData(field=field)) is True


def test_is_select_multiple():
    field = forms.ChoiceField(widget=forms.SelectMultiple)
    assert is_select_multiple(FormData(field=field)) is True


@pytest.mark.parametrize(
    "tag_function_name",
    [
        is_checkbox,
        is_checkbox_select_multiple,
        is_file_input,
        is_radio_select,
        is_select_multiple,
    ],
)
def test_not(tag_function_name):
    field = forms.CharField()
    assert tag_function_name(FormData(field=field)) is False
