# -*- encoding: utf-8 -*-
from bleach import clean, ALLOWED_TAGS, ALLOWED_ATTRIBUTES
from django import forms
from django.forms.utils import flatatt
from django.forms.widgets import FileInput
from django.utils.encoding import force_str
from django.utils.safestring import mark_safe
from pathlib import Path


def bleach_clean(data, strip=None):
    """Use bleach to clean up html.

    Keyword arguments:
    strip -- remove html tags rather than escaping them.

    """
    attributes = ALLOWED_ATTRIBUTES
    attributes.update(
        {
            # link
            "a": ["href", "target"],
            # YouTube
            "iframe": [
                "allowfullscreen",
                "frameborder",
                "height",
                "src",
                "width",
            ],
            # Image
            "img": ["alt", "src", "style"],
            # Text color
            "span": ["style"],
        }
    )
    styles = ["float", "height", "width", "color", "background-color"]
    tags = ALLOWED_TAGS + [
        "br",
        "iframe",
        "img",
        "p",
        "table",
        "tbody",
        "td",
        "thead",
        "tr",
        "u",
        "span",
    ]
    params = dict(tags=tags, attributes=attributes, styles=styles)
    if strip:
        params.update(dict(strip=True))
    return clean(data, **params)


def set_widget_required(field):
    """Why do I need to call this when setting 'required' on the form."""
    field.widget.attrs.update(
        {"required": None, "placeholder": "This is a required field"}
    )
    field.required = True


class FileDropInput(FileInput):
    def __init__(self, **kwargs):
        self.default_text = kwargs.pop("default_text", "Drop a file...")
        self.click_text = kwargs.pop("click_text", "or click here...")
        self.current_file_text = kwargs.pop(
            "current_file_text", "Current file:"
        )
        self.zone_id = kwargs.pop("zone_id", "filedrop-zone")
        self.file_name_class = ""
        self.click_class = ""
        super().__init__(**kwargs)

    def render(self, name, value, attrs=None, renderer=None):
        filedrop_class = ""
        current_file = ""
        if not value:
            value = ""
        extra_attrs = attrs.copy() if attrs else {}
        extra_attrs.update(name=name, type=self.input_type)
        input_attrs = self.build_attrs(self.attrs, extra_attrs)
        if input_attrs:
            # remove the attributes that are not for the input tag
            fd_class = input_attrs.get("class", None)
            if fd_class:
                filedrop_class = 'class="{}"'.format(fd_class)
                del input_attrs["class"]
            if "click_text" in input_attrs:
                # attrs was updated in the form use the new value
                self.click_text = input_attrs["click_text"]
                del input_attrs["click_text"]
            if "current_file_text" in input_attrs:
                # attrs was updated in the form use the new value
                self.current_file_text = input_attrs["current_file_text"]
                del input_attrs["current_file_text"]
            if "default_text" in input_attrs:
                # attrs was updated in the form use the new value
                self.default_text = input_attrs["default_text"]
                del input_attrs["default_text"]
            input_attrs["data-default-text"] = force_str(self.default_text)
            if value != "":
                # Only add the 'value' attribute if a value is non-empty.
                input_attrs["value"] = force_str(self.format_value(value))
                current_file = "{} '{}'".format(
                    self.current_file_text, Path(value.name).name
                )
        return mark_safe(
            (
                '<div id="{}" {}>'
                '<span class="filedrop-file-name">{}</span>'
                '<div class="filedrop-click-here">'
                "{}"
                "<input {}>"
                "</div>"
                "</div>"
            ).format(
                self.zone_id,
                filedrop_class,
                "{}{}".format(self.default_text, current_file),
                self.click_text,
                flatatt(input_attrs),
            )
        )


class RequiredFieldForm(forms.ModelForm):
    """Add the 'required' attribute"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        file_fields = 0
        for name in self.fields:
            f = self.fields[name]
            if f.required:
                set_widget_required(f)
            if isinstance(f, forms.DateField):
                f.widget.attrs.update({"class": "datepicker"})
            if isinstance(f, forms.FileField) or isinstance(
                f, forms.ImageField
            ):
                if file_fields > 0:
                    f.widget = FileDropInput(
                        zone_id="filedrop-{}-zone".format(file_fields)
                    )
                else:
                    f.widget = FileDropInput()
                file_fields += 1
