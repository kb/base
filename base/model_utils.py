# -*- encoding: utf-8 -*-
import logging

from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.db import DatabaseError, models, transaction
from django.db.models import AutoField, F, Max
from django.utils import timezone

logger = logging.getLogger(__name__)

# We want attachments to be stored in a private location and NOT available to
# the world at a public URL.  The idea for this came from:
# http://nemesisdesign.net/blog/coding/django-private-file-upload-and-serving/
# and
# https://github.com/johnsensible/django-sendfile
private_file_store = FileSystemStorage(location=settings.SENDFILE_ROOT)


ftp_file_store = FileSystemStorage(
    location=settings.FTP_STATIC_DIR, base_url=settings.FTP_STATIC_URL
)


class BaseError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("%s, %s" % (self.__class__.__name__, self.value))


def _get_model_instance_data(obj):
    """
    Helper function for 'copy_model_instance'.
    """
    return dict(
        [
            (f.name, getattr(obj, f.name))
            for f in obj._meta.fields
            if not isinstance(f, AutoField)
            and not f in obj._meta.parents.values()
        ]
    )


def copy_model_instance(obj):
    """
    Copy field values from a model to a new instance.

    Code copied from:
    - http://djangosnippets.org/snippets/1040/
    - http://code.djangoproject.com/ticket/4027
    """
    initial = _get_model_instance_data(obj)
    return obj.__class__(**initial)


def copy_model_instance_to(from_obj, to_class):
    """
    Copy the data for a model instance to a new class which has the same
    fields.

    My variation on the `copy_model_instance` function (above)...
    """
    initial = _get_model_instance_data(from_obj)
    return to_class(**initial)


class RetryModelManager(models.Manager):
    """Handle a retry operation.

    Use with the ``RetryModel`` (see below)...

    .. note:: Initial code copied from ``report/models.py``.

    """

    def _clazz_name(self):
        return "{}".format(self.__class__.__name__)

    def _process_item(self, item):
        result = False
        try:
            result = item.process()
        except Exception:
            logger.exception("Cannot process: {}".format(self._clazz_name()))
        if result:
            item.set_complete()
        else:
            item.refresh_from_db()
            if item.is_complete():
                pass
            else:
                item.set_fail()
        return result

    def completed(self):
        qs = self.model.objects.current().filter(completed_date__isnull=False)
        return qs.order_by("completed_date")

    def outstanding(self, queue_name=None):
        """Outstanding items.

        Keyword arguments:
        queue_name -- only outstanding items with the queue name (default None)

        """
        qs = self.model.objects.current()
        qs = qs.filter(
            completed_date__isnull=True, retries__lt=F("max_retry_count")
        )
        if queue_name:
            qs = qs.filter(queue_name=queue_name)
        else:
            qs = qs.filter(queue_name="")
        return qs.order_by("pk")

    def outstanding_ignore_retries(self):
        """Outstanding items - ignoring retries."""
        return self.model.objects.current().filter(completed_date__isnull=True)

    def process(self, pk=None, queue_name=None):
        """Process the outstanding items.

        Keyword arguments:
        pk -- process a single item (default None)
        queue_name -- process only items matching the queue name (default None)

        .. tip: If this method is run twice in quick succession and each item
                takes some time to process, then items will appear in the list
                of outstanding items.
                To try and solve this, we double check the item can still be
                processed after calling ``select_for_update``.

        """
        result = []
        if pk is None:
            pks = [x.pk for x in self.outstanding(queue_name)]
        else:
            pks = [pk]
        for pk in pks:
            with transaction.atomic():
                try:
                    # lock, so the function is only called once
                    item = self.model.objects.select_for_update(
                        nowait=True
                    ).get(pk=pk)
                    # check the item is still outstanding
                    if item.can_process() and self._process_item(item):
                        result.append(item.pk)
                except DatabaseError:
                    # record is locked, so leave alone this time
                    pass
        return result


class RetryModel(models.Model):
    """Handle a retry operation.

    1. Your model manager should inherit from ``RetryModelManager``.
    2. Create a ``current`` method in your model manager:

       ::

         class MyModelManager(RetryModelManager):
             def current(self):
                 return self.model.objects.all()

    3. When you create an instance of your model, you will need to set the
       ``max_retry_count``.  The ``max_retry_count`` is a field in the model so
       it can be increased to try again.

    .. note:: Initial code copied from ``report/models.py``.

    """

    DEFAULT_MAX_RETRY_COUNT = 5

    completed_date = models.DateTimeField(blank=True, null=True)
    fail_date = models.DateTimeField(blank=True, null=True)
    max_retry_count = models.PositiveIntegerField()
    queue_name = models.CharField(max_length=200, blank=True)
    retries = models.PositiveIntegerField(default=0)

    class Meta:
        abstract = True

    def can_process(self):
        result = False
        if self.is_complete():
            pass
        elif self.retries < self.max_retry_count:
            result = True
        return result

    def is_complete(self):
        return bool(self.completed_date)

    def retry_str(self):
        if self.is_complete():
            result = "completed {}".format(
                self.completed_date.strftime("%d/%m/%Y %H:%M")
            )
        elif self.retries:
            result = "retry x {}".format(self.retries)
        else:
            result = ""
        return result

    def set_complete(self):
        self.completed_date = timezone.now()
        self.save()

    def set_fail(self):
        self.retries = self.retries + 1
        self.fail_date = timezone.now()
        self.save()


class TimeStampedModel(models.Model):
    """
    An abstract base class model that provides self-updating
    ``created`` and ``modified`` fields
    """

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class TimedCreateModifyDeleteModel(TimeStampedModel):
    """
    An abstract base class model that provides self-updating
    ``created`` and ``modified`` fields
    """

    deleted = models.BooleanField(default=False)
    date_deleted = models.DateTimeField(blank=True, null=True)
    user_deleted = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name="+",
    )

    def undelete(self):
        if not self.is_deleted:
            raise BaseError(
                "Cannot undelete '{}', pk '{}'.  It is not "
                "deleted".format(self.__class__.__name__, self.pk)
            )
        self.deleted = False
        self.date_deleted = None
        self.user_deleted = None
        self.save()

    @property
    def is_deleted(self):
        return self.deleted

    def set_deleted(self, user):
        if self.is_deleted:
            raise BaseError(
                "Cannot delete '{}', pk '{}'.  It is already "
                "deleted".format(self.__class__.__name__, self.pk)
            )
        self.deleted = True
        self.date_deleted = timezone.now()
        self.user_deleted = user
        self.save()

    class Meta:
        abstract = True


class TimedCreateModifyDeleteArchiveModel(TimedCreateModifyDeleteModel):
    archived = models.BooleanField(default=False)

    @property
    def is_archived(self):
        return self.archived

    def set_archived(self):
        if self.is_archived:
            raise BaseError(
                "Cannot archived '{}', pk '{}'.  It is already "
                "archived".format(self.__class__.__name__, self.pk)
            )
        self.archived = True
        self.save()

    def unarchive(self):
        if not self.is_archived:
            raise BaseError(
                "Cannot unarchive '{}', pk '{}'.  It is not "
                "archived".format(self.__class__.__name__, self.pk)
            )
        self.archived = False
        self.save()

    class Meta:
        abstract = True


class TimedCreateModifyDeleteVersionModelManager(models.Manager):
    # check the last 20 numbers to see which ones can be re-used
    REUSE_X_NUMBERS = 20

    def _max_deleted_version(self, obj):
        field_name = self.model.unique_field_name()
        unique_value = getattr(obj, field_name)
        kwargs = {field_name: unique_value}
        qs = self.model.objects.filter(**kwargs)
        result = qs.aggregate(max_id=Max("deleted_version"))
        return result.get("max_id") or 0

    def next_number(self, category=None):
        result = None
        qs = self.model.objects.all()
        if category:
            field_name_category = self.model.unique_field_name_category()
            qs = qs.filter(**{field_name_category: category})
        current = []
        complete_list = []
        field_name = self.model.unique_field_name()
        for row in qs.order_by(field_name)[: self.REUSE_X_NUMBERS]:
            complete_list.append(row.number)
            if not row.deleted:
                current.append(row.number)
        missing = [x for x in complete_list if not x in current]
        if missing:
            result = missing[0]
        if not result:
            row = qs.aggregate(max_id=Max(field_name))
            current_number = row.get("max_id") or 0
            result = current_number + 1
        if not result:
            raise BaseError(
                "Cannot get next number for '{}'".format(self.model)
            )
        return result

    def set_deleted(self, obj, user):
        max_deleted_version = self._max_deleted_version(obj)
        obj.set_deleted(user, max_deleted_version)


class TimedCreateModifyDeleteVersionModel(TimedCreateModifyDeleteModel):
    """Keep track of deleted versions.

    Written to solve the problem of re-using invoice numbers without having
    to properly delete a row.

    The ``deleted_version`` is incremented each time the unique field (e.g.
    invoice number) is deleted e.g. if you have an invoice number 3 and you
    delete it once, the ``deleted_version`` will be set to `.  If you create
    a new invoice number 3 and delete it, the ``deleted_version`` will be set
    to 2.

    The class has the option to re-use numbers within a category.  This allows
    two sets of numbers per model e.g. "internal" and "external".

    To use the class::

      # 1. inherit from 'TimedCreateModifyDeleteVersionModel'
      class Invoice(TimedCreateModifyDeleteVersionModel):

          # 2a. set the unique field name for the model e.g. invoice number
          UNIQUE_FIELD_NAME = 'number'

          # if you are not using a category, then set this field value to None
          UNIQUE_FIELD_NAME_CATEGORY = None

          # 2b. (optional) set the field name for your category e.g.
          UNIQUE_FIELD_NAME_CATEGORY = 'category'

          # 3a. create your unique field e.g. invoice number.  Don't set the
          #     ``unique`` attribute as we use ``unique_together``
          number = models.IntegerField(default=0)

          # 3b. (optional) create your category field
          category = models.CharField(max_length=10)

          class Meta:
              # 4a. create a unique index on the field and 'deleted_version'
              unique_together = ('number', 'deleted_version')

              # 4b. (optional) if you are using a category, include it:
              unique_together = ('number', 'deleted_version', 'category')

    To assign a unique number to your model instance (perhaps in the
    ``create_`` method on the model manager)::

      obj = Invoice(user=user)
      obj.number = self.next_number()

      # if you are using a category, then use this version:
      obj = Invoice(user=user, category=category)
      obj.number = self.next_number(category)

    Use the model manager to delete a row from the ``Invoice`` model::

      invoice = InvoiceFactory()
      Invoice.objects.set_deleted(obj, user)

    """

    deleted_version = models.IntegerField(default=0)

    def undelete(self):
        self.deleted_version = 0
        super().undelete()

    @classmethod
    def unique_field_name(cls):
        try:
            return cls.UNIQUE_FIELD_NAME
        except AttributeError:
            raise BaseError(
                "'{}': needs a 'UNIQUE_FIELD_NAME' to find the "
                "'deleted_version' (see 'TimedCreateModifyDeleteVersionModel' "
                "for more information)".format(cls)
            )

    @classmethod
    def unique_field_name_category(cls):
        try:
            return cls.UNIQUE_FIELD_NAME_CATEGORY
        except AttributeError:
            raise BaseError(
                "'{}': needs a 'UNIQUE_FIELD_NAME_CATEGORY' to find the "
                "'deleted_version' (see 'TimedCreateModifyDeleteVersionModel' "
                "for more information)".format(cls)
            )

    def set_deleted(self, user, max_deleted_version=None):
        if max_deleted_version is None:
            raise BaseError(
                "'TimedCreateModifyDeleteVersionModel' needs a "
                "'max_deleted_version' to set the 'deleted_version'. "
                "Use the 'set_deleted' method in the model manager for "
                "a model with deleted version tracking."
            )
        self.deleted_version = max_deleted_version + 1
        super().set_deleted(user)

    class Meta:
        abstract = True
