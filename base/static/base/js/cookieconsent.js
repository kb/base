// const cookieconsent = {

  /*
  const cookieStorage = {
    getItem: (item) => {
      const cookies = document.cookie
        .split(';')
        .map(cookie => cookie.split('='))
        .reduce((acc, [key, value]) => ({ ...acc, [key.trim()]: value }), {});
      return cookies[item];
    },
    setItem: (item, value) => {
      document.cookie = `${item}=${value};`
    }
  }
  */

  const storageType = localStorage;
  const consentPropertyName = 'cookie_consent';
  const shouldShowPopup = () => !storageType.getItem(consentPropertyName);
  const saveToStorage = () => storageType.setItem(consentPropertyName, true);

  window.addEventListener('load', (evt) => {
    const consentPopup = document.getElementById('cookie-consent-popup');
    const acceptBtn = document.getElementById('cookie-consent-accept');
    acceptBtn.addEventListener('click', (evt) => {
      saveToStorage(storageType);
      consentPopup.classList.add('hidden');
    });

    if (shouldShowPopup(storageType)) {
      setTimeout(() => {
        consentPopup.classList.remove('hidden')
      }, 2000);
    }
  }, false);
// };
