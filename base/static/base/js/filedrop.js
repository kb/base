$(function() {
function configureFileDropZone (dropZoneId="filedrop-zone") {
  var mouseOverClass = "mouse-over";
  var defaultText =  "Drop your file...";

  var dropZone = $('#' + dropZoneId);
  if (dropZone.length) {
    var clickZone = dropZone.find(".filedrop-click-here")
    var fileNameLabel = dropZone.find(".filedrop-file-name")
    var inputFile = dropZone.find("input[type='file']");

    clickZone.mousemove(function (e) {
        var x = e.pageX;
        var y = e.pageY;
        var oleft = clickZone.offset().left;
        var oright = clickZone.outerWidth() + oleft;
        var otop = clickZone.offset().top;
        var obottom = clickZone.outerHeight() + otop;

        if (!(x < oleft || x > oright || y < otop || y > obottom)) {
            inputFile.offset({ top: y - 15, left: x - 160 });
        } else {
            inputFile.offset({ top: -400, left: -400 });
        }
    });

    dropZone.on("dragover", function(e) {
        e.preventDefault();
        e.stopPropagation();
        dropZone.addClass(mouseOverClass);
        var ooleft = dropZone.offset().left;
        var ooright = dropZone.outerWidth() + ooleft;
        var ootop = dropZone.offset().top;
        var oobottom = dropZone.outerHeight() + ootop;

        var x = e.originalEvent.pageX;
        var y = e.originalEvent.pageY;

        if (!(x < ooleft || x > ooright || y < ootop || y > oobottom)) {
            inputFile.offset({ top: y - 15, left: x - 100 });
        } else {
            inputFile.offset({ top: -400, left: -400 });
        }
    });

    inputFile.on('change', function(e) {
      var filePath = inputFile.val();
      if (filePath) {
        var fileName = filePath.replace(/^.*[\\\/]/, '');
        fileNameLabel.text(fileName);
      }
      else {
        var defText = inputFile.data('default-text');
        if (defText === undefined)
          defText = defaultText;
        fileNameLabel.text(defText);
      }
    });

    dropZone.on("drop", function(e){
        dropZone.removeClass(mouseOverClass);
    });
  }
}
configureFileDropZone();
configureFileDropZone('filedrop-1-zone');
configureFileDropZone('filedrop-2-zone');
configureFileDropZone('filedrop-3-zone');
});
