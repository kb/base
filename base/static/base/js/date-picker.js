$(document).ready(function() {
  // attach to controls marked with the "datepicker" class
  $('input.datepicker').Zebra_DatePicker({
    format: 'd/m/Y'
  });
  // Set the focus to the first input element in an HTML form
  $('form:first *:input[type!=hidden]:first').focus();
});
