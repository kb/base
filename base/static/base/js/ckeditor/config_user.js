/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    // Define changes to default configuration here.
    // For complete reference see:
    // http://docs.ckeditor.com/#!/api/CKEDITOR.config

    config.toolbar = [
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
        { name: 'editing', groups: [ 'find', 'selection', ], items: [ 'Find', 'Replace', '-', 'SelectAll' ] },
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', '-', 'RemoveFormat' ] },
        { name: 'paragraph', groups: [ 'list', 'indent' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent' ] }
    ]

    // The toolbar groups arrangement, optimized for two toolbar rows.
    config.toolbarGroups = [
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
        { name: 'editing', groups: [ 'find', 'selection' ] },
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'paragraph', groups: [ 'list', 'indent' ] },
    ];

    config.removePlugins = 'elementspath';
    config.resize_enabled = false;

    // Set the most common block elements.
    config.format_tags = 'p;h1;h2;h3;pre';

	config.contentsCss = [ CKEDITOR.getUrl('contents.css'), '/static/base/css/ckeditor-extra-styles.css' ];


    // Make the allowedContent the same as base.form_utils.bleach_clean - except no iframe and img
    config.allowedContent = 'a[*]; abbr; acronym; b; blockquote; br; code; em; i; li; ol; p; span{color,background-color}; strong; table; tbody; td; thead; tr; u; ul;';
};

