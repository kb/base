/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    // Define changes to default configuration here.
    // For complete reference see:
    // http://docs.ckeditor.com/#!/api/CKEDITOR.config

    // // The toolbar groups arrangement, optimized for two toolbar rows.
    config.toolbarGroups = [
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
        { name: 'editing', groups: [ 'find', 'selection' ] },
        { name: 'links' },
        { name: 'insert' },
        { name: 'forms' },
        { name: 'tools' },  // maximise
        { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
        { name: 'others' },
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
        { name: 'styles' },
        { name: 'colors' },
        { name: 'about' }
    ];

    config.extraPlugins = 'youtube,panelbutton,colorbutton,colordialog';

    config.colorButton_enableMore = true;
    config.colorButton_enableAutomatic = true;

    // config.colorButton_backStyle.element = {
    //     element: 'font',
    //     styles: { background-color: '#(color)' }
    // };
    // config.colorButton_foreStyle = {
    //     element: 'span',
    //     styles: { color: '#(color) !important' }
    // };

    // Remove some buttons provided by the standard plugins, which are
    // not needed in the Standard(s) toolbar.
    config.removeButtons = 'About,Anchor,Blockquote,Format,HorizontalRule,Image,Source,Strike,Styles,Subscript,Superscript,Table';

    config.disableNativeSpellChecker = false;
    // Set the most common block elements.
    config.format_tags = 'p;h1;h2;h3;pre';
    
   	config.contentsCss = [ CKEDITOR.getUrl('contents.css'), '/static/base/css/ckeditor-extra-styles.css' ];

    // Simplify the dialog windows.
    config.removeDialogTabs = 'image:advanced;link:advanced';

    // Make the allowedContent the same as base.form_utils.bleach_clean - except no iframe and img
    config.allowedContent = 'a[*]; abbr; acronym; b; blockquote; br; code; em; i; li; ol; p; span{color,background-color}; strong; table; tbody; td; thead; tr; u; ul;';
    // config.extraAllowedContent = 'font';
};
