let modalCarousel = (function (modalName) {
  const modal = document.querySelector(modalName);
  const closeButton = modal.querySelector('.carousel-button-close');
  const indicators = modal.querySelector('.carousel-indicators');
  const carousel = modal.querySelector("#carousel");
  const track = carousel.querySelector('#carousel-track');
  const nextButton = carousel.querySelector('.carousel-button-right');
  const prevButton = carousel.querySelector('.carousel-button-left');

  let slides = undefined;
  let slideWidth = 0
  let dots = undefined;

  const setSlidePosition = (slide, index) => {
    slide.style.left = slideWidth * index + 'px';
  }

  const moveToSlide = (track, currentSlide, targetSlide) => {
    track.style.transform = 'translateX(-' + targetSlide.style.left + ')';
    currentSlide.classList.remove("current-slide");
    targetSlide.classList.add("current-slide");
  }

  const updateIndicators = (currentDot, targetDot) => {
    currentDot.classList.remove('current-slide');
    targetDot.classList.add('current-slide');
  }

  const showHideArrows = (slides, prevButton, nextButton, targetIndex) => {
    if (targetIndex === 0) {
      prevButton.classList.add('is-hidden');
      nextButton.classList.remove('is-hidden');
    }
    else if (targetIndex === slides.length -1) {
      prevButton.classList.remove('is-hidden');
      nextButton.classList.add('is-hidden');
    }
    else {
      prevButton.classList.remove('is-hidden');
      nextButton.classList.remove('is-hidden');
    }
  }

  closeButton.addEventListener('click', e => {
    hideModal();
  });

  nextButton.addEventListener('click', e => {
    const currentSlide = track.querySelector('.current-slide');
    const targetSlide = currentSlide.nextElementSibling;
    const currentDot = indicators.querySelector(".current-slide");
    const targetDot = currentDot.nextElementSibling;
    const targetIndex = slides.findIndex(slide => slide == targetSlide);
    moveToSlide(track, currentSlide, targetSlide);
    updateIndicators(currentDot, targetDot);
    showHideArrows(slides, prevButton, nextButton, targetIndex);
  });

  prevButton.addEventListener('click', e => {
    const currentSlide = track.querySelector('.current-slide');
    const targetSlide = currentSlide.previousElementSibling;
    const currentDot = indicators.querySelector(".current-slide");
    const targetDot = currentDot.previousElementSibling;
    const targetIndex = slides.findIndex(slide => slide == targetSlide);

    moveToSlide(track, currentSlide, targetSlide);
    updateIndicators(currentDot, targetDot);
    showHideArrows(slides, prevButton, nextButton, targetIndex);
  });

  const addCarouselItem = (track, indicators, idx, imgSrc) => { 
    // add the image
    const item = document.createElement("li");
    item.className = "carousel-slide";
    const img = document.createElement("img");
    img.classList.add("carousel-image");
    img.src = imgSrc;
    img.alt = "";
    item.appendChild(img);
    // add the indicator
    const indicator = document.createElement("button");
    indicator.classList.add("carousel-indicator");
    if (idx === 0) {
      item.classList.add("current-slide");
      indicator.classList.add("current-slide");
    }
    track.appendChild(item);
    indicators.appendChild(indicator);
  }

  const setupCarouselItems = (e) => {
    deleteCarouselItems();
    const imagesStr = e.target.getAttribute("data-images");
    const imgs = imagesStr.split(',');
    console.log(imgs);
    for (let i = 0; i < imgs.length; i++) {
      addCarouselItem(track, indicators, i, imgs[i]);
    }
    slides = Array.from(track.children);
    slideWidth = slides[0].getBoundingClientRect().width;
    slides.forEach(setSlidePosition);
    dots = Array.from(indicators.children);

    // moveToSlide(track, currentSlide, targetSlide);
    // updateIndicators(currentDot, targetDot);
    showHideArrows(slides, prevButton, nextButton, 0);
    track.style.transform = '';
    indicators.addEventListener('click', e => {
      const targetDot = e.target.closest('button');
      if (!targetDot) return;
      const currentSlide = track.querySelector('.current-slide');
      const currentDot = indicators.querySelector('.current-slide');
      const targetIndex = dots.findIndex(dot =>  dot === targetDot);
      const targetSlide = slides[targetIndex];

      moveToSlide(track, currentSlide, targetSlide);
      updateIndicators(currentDot, targetDot);
      showHideArrows(slides, prevButton, nextButton, targetIndex);
    });
  }

  const deleteCarouselItems = () => {
    while(track.firstChild) track.removeChild(track.lastChild);
    while(indicators.firstChild) indicators.removeChild(indicators.lastChild);
  }

  const showModal = () => {
    modal.style.display = 'block';
  }

  const hideModal = () => {
    modal.style.display = 'none';
  }

  const isModalVisible = () => {
    return modal.style.display === 'block';
  }

  const setupAndShow = (e) => {
    console.log("setup and show");
    showModal();
    setupCarouselItems(e);
  }

  return {
    hide: hideModal,
    isVisible: isModalVisible,
    setupItems: setupCarouselItems,
    setupShow: setupAndShow,
    show: showModal,
  };

})("#images-modal");
