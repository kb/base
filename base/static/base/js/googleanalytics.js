// Usage - add the following to the base template for your site
//
// (N.B. replace `{{ GA4_tag }}` with the tag of your GA4 property)
// <script async src="https://www.googletagmanager.com/gtag/js?id={{ GA4_tag }}"></script>
// <script async src="{% static 'base/js/googleanalytics.js' %}" data-tag="{{ GA4_tag }}" data-domain="{{ domain e.g. www.domain.com }}"></script>
if (navigator.doNotTrack != 1) {
  var gTagID = document.currentScript.dataset.tag;
  var gCookieDomain = document.currentScript.dataset.domain;
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', gTagID, {cookie_flags: 'SameSite=None;Secure', cookie_domain: gCookieDomain});
}
