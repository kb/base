// bxslider initialisation script
// use as follows:
// <script src="{% static 'vendor/js/jquery.bxslider.js' %}"></script>
// <script src="{% static 'base/js/bxslider.js' %}"> </script>
$(document).ready(function(){
  $('.bxslider').bxSlider({
    auto: true,
    autoHover: true,
    controls: false,
    pager: false,
    pause: 10000,
    speed: 1000,
    useCSS: false
  });
});
