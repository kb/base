"""
Template tag to removes whitespace between HTML tags, e.g. tab and newline,
only if settings.DEBUG = False

Only space between *tags* is normalized -- not space between tags and text.

Usage:
    {% load smartspaceless %}
    {% smartspaceless %}
    <p>
        <a href="foo/">Foo</a>
    </p>
    {% endsmartspaceless %}

This example would return this HTML:

    <p><a href="foo/">Foo</a></p>

"""

from django.conf import settings
from django import template
from django.utils.html import strip_spaces_between_tags

register = template.Library()

class SmartSpacelessNode(template.Node):
    def __init__(self, nodelist):
        self.nodelist = nodelist

    def render(self, context):
        content = self.nodelist.render(context)
        return content if settings.DEBUG else strip_spaces_between_tags(content.strip())

@register.tag
def smartspaceless(parser, token):
    nodelist = parser.parse(('endsmartspaceless',))
    parser.delete_first_token()
    return SmartSpacelessNode(nodelist)
