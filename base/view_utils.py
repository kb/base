# -*- encoding: utf-8 -*-
import importlib
import time

from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.utils import timezone
from django.utils.encoding import iri_to_uri
from django.utils.http import url_has_allowed_host_and_scheme


class BaseError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("{}, {}".format(self.__class__.__name__, self.value))


def import_class(plugin_class_name):
    pos = plugin_class_name.rfind(".")
    if pos == -1:
        raise BaseError(
            "Cannot find '{}' (no '.' in the class name)".format(
                plugin_class_name
            )
        )
    module_name = plugin_class_name[:pos]
    plugin_module = import_module(module_name)
    plugin_class = plugin_class_name[pos + 1 :]
    return getattr(plugin_module, plugin_class)


def import_module(module_name):
    try:
        plugin_module = importlib.import_module(module_name)
    except ImportError as e:
        # ``ModuleNotFoundError`` not available until python 3.6
        message = "Cannot find '{}' ({})".format(module_name, str(e))
        raise BaseError(message)
    return plugin_module


def plugin_classes(plugin_class_list):
    plugin_classes = []
    for plugin_class_name in plugin_class_list:
        plugin_classes.append(import_class(plugin_class_name))
    return plugin_classes


def request_redirect_url(request):
    result = None
    next_url = request.GET.get(REDIRECT_FIELD_NAME)
    if not next_url:
        if request.method == "POST":
            next_url = request.POST.get(REDIRECT_FIELD_NAME)
    if next_url and url_has_allowed_host_and_scheme(
        next_url, allowed_hosts=None
    ):
        result = next_url
    return result


class BaseMixin:
    current_app_slug = None

    def get_context_data(self, **kwargs):
        """Base context for our views.

        Update the context with the data from the plugin classes:
        https://www.kbsoftware.co.uk/crm/ticket/4777/

        """
        context = super().get_context_data(**kwargs)
        # plugin classes (if available in 'settings')
        try:
            plugin_class_list = settings.BASE_MIXIN_CONTEXT_PLUGIN
        except AttributeError:
            plugin_class_list = []
        for clazz in plugin_classes(plugin_class_list):
            context.update(clazz.get_context_data(self))
        # default context
        context.update(
            dict(
                path=get_path(self.request.path),
                request_path=self.request.path,
                testing=settings.TESTING,
                today=timezone.now(),
            )
        )
        return context


def get_path(path):
    """Path processing can be used by other views."""
    result = path
    if result == "/":
        result = "home"
    return result


class DateSeriesBarChartMixin(object):
    """Create a single series bar chart
    In your template include the template snippet

    {% include 'base/_date_series_bar_chart.html' %}

    or a template derived from this one

    in you get_context_data:
        context = super().get_context_data(**kwargs)
        context.update(
            chartdata=self.get_chart_data(
                container="<container name>",
                legend="<series legend>",
                raw_data=[{'x': <date 1>, 'y': value}, ...],
                date_format="<date format e.g."%b %Y">
            )
        )
        return context

    the css for your container should include the following:
    <id of container> {
        width: <required width>px;
        height: <required height>px;
    }
    """

    def get_chart_data(self, container, legend, raw_data, date_format):
        data = []
        for key, value in raw_data:
            data.append(
                {"x": int(time.mktime(key.timetuple()) * 1000), "y": value}
            )

        return {
            "container": container,
            "legend": legend,
            "data": data,
            "date_format": date_format,
        }


class ElasticPaginatorPage:
    """

    .. tip:: Works with ``ElasticPaginator`` (see below).

    """

    def __init__(self, page_size, total):
        self.page_size = page_size
        self.total = total

    @property
    def num_pages(self):
        return -(-self.total // self.page_size)


class ElasticPaginator:
    """Paginate ElasticSearch results.

    .. tip:: Works with ``ElasticPaginatorPage`` (see above).

    Original code copied from:
    https://gitlab.com/kb/job/blob/c278065720bb3871fcd41e76f9a549be7a7a4778/job/views.py#L1003

    """

    def __init__(self, page_number, page_size, total):
        self.page_number = page_number
        self.page_size = page_size
        self.total = total

    @property
    def has_next(self):
        result = False
        if self.has_other_pages():
            count = self.page_number * self.page_size
            result = self.total > count
        return result

    def has_other_pages(self):
        return self.total > self.page_size

    @property
    def has_previous(self):
        return self.has_other_pages() and self.page_number > 1

    @property
    def next_page_number(self):
        result = self.page_number
        if self.has_next:
            result = self.page_number + 1
        return result

    @property
    def number(self):
        return self.page_number

    @property
    def paginator(self):
        return ElasticPaginatorPage(self.page_size, self.total)

    @property
    def previous_page_number(self):
        result = 1
        if self.page_number > 1:
            result = self.page_number - 1
        return result


class RedirectNextMixin:
    """Handle the 'next' parameter on a URL.

    For details, see
    https://www.kbsoftware.co.uk/docs/app-base.html#redirectnextmixin

    """

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        next_url = request_redirect_url(self.request)
        if next_url:
            context.update({REDIRECT_FIELD_NAME: iri_to_uri(next_url)})
        return context
